import unittest
from app import app

class TestApp(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()

    def test_root_endpoint(self):
        response = self.app.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data.decode('utf-8'), 'Привет, мир! Это мой веб-сервер.')

    # Добавьте здесь другие тесты в соответствии с вашим приложением

if __name__ == '__main__':
    unittest.main()
