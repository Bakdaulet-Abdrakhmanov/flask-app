FROM python:3.6.5

WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
EXPOSE 80
COPY . .


ENTRYPOINT ["python", "app.py"]
